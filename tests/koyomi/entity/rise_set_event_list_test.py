import datetime
import pytest

from naoj.koyomi.entity.rise_set_event import RiseSetEvent
from naoj.koyomi.entity.rise_set_event_list import RiseSetEventList


@pytest.fixture(scope='function', autouse=True)
def event():
    yield RiseSetEvent(
        datetime=datetime.datetime(2022, 1, 1),
        place="tokyo",
        event="rise"
    )


class Test__Add__:
    @pytest.mark.parametrize("testcase, input, expect", [
        ("RiseSetEventList", "<RiseSetEventList([event, event])>", "pass"),
        ("list[RiseSetEvent]", "<[event, event]>", "pass"),
        ("RiseSetEvent", "<event>", "raise"),
        ("list[str]", ["event", "event"], "raise"),
        ("str", "event", "raise"),
    ])
    def test_validation(self, event, testcase, input, expect):
        # given
        events = RiseSetEventList()
        # overwrite input
        if input == "<RiseSetEventList([event, event])>":
            input = RiseSetEventList([event, event])
        elif input == "<[event, event]>":
            input = [event, event]
        elif input == "<event>":
            input = event
        # when # then
        if expect == "raise":
            with pytest.raises(TypeError):
                events = events + input
        else:
            events = events + input

    @pytest.mark.parametrize("testcase, llen, rren", [
        ("0_and_0", 0, 0),
        ("0_and_more_than_0", 0, 3),
        ("more_than_0_and_0", 4, 0),
        ("more_than_0_and_more_than_0", 4, 3),
    ])
    def test_regular(self, event, testcase, llen, rren):
        # given
        levents = RiseSetEventList()
        revents = RiseSetEventList()
        # when
        [levents.append(event) for _ in range(llen)]
        revents = revents + [event for _ in range(rren)]
        # then
        assert len(levents) == llen
        assert len(revents) == rren
        # when
        new_events = levents + revents
        # then
        assert len(new_events) == llen + rren
        assert len(levents) == llen
        assert len(revents) == rren


class Test__Iadd__:
    @pytest.mark.parametrize("testcase, input, expect", [
        ("RiseSetEventList", "<RiseSetEventList([event, event])>", "pass"),
        ("list[RiseSetEvent]", "<[event, event]>", "pass"),
        ("RiseSetEvent", "<event>", "raise"),
        ("list[str]", ["event", "event"], "raise"),
        ("str", "event", "raise"),
    ])
    def test_validation(self, event, testcase, input, expect):
        # given
        events = RiseSetEventList()
        # overwrite input
        if input == "<RiseSetEventList([event, event])>":
            input = RiseSetEventList([event, event])
        elif input == "<[event, event]>":
            input = [event, event]
        elif input == "<event>":
            input = event
        # when # then
        if expect == "raise":
            with pytest.raises(TypeError):
                events += input
        else:
            events += input

    @pytest.mark.parametrize("testcase, llen, rren", [
        ("0_and_0", 0, 0),
        ("0_and_more_than_0", 0, 3),
        ("more_than_0_and_0", 4, 0),
        ("more_than_0_and_more_than_0", 4, 3),
    ])
    def test_regular(self, event, testcase, llen, rren):
        # given
        levents = RiseSetEventList()
        revents = RiseSetEventList()
        # when
        levents.extend([event for _ in range(llen)])
        revents += [event for _ in range(rren)]
        # then
        assert len(levents) == llen
        assert len(revents) == rren
        # when
        levents += revents
        # then
        assert len(levents) == llen + rren
        assert len(revents) == rren
