import pytest

from naoj.koyomi.web.rise_set_web_repository import RiseSetWebRepository

ENABLE_TEST = True
SKIP_IF = pytest.mark.skipif(
    not ENABLE_TEST,
    reason='access to naj server should be limited',
)


@SKIP_IF
class TestGetMoonRiseSetEvents:
    def test_get_moon_rise_set_events(self):
        # given
        repo = RiseSetWebRepository()

        # when
        events = repo.get_moon_rise_set_events()

        # then
        assert len(events) == 60
