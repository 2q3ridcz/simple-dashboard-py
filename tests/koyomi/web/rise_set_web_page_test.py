import pathlib

from naoj.koyomi.web.rise_set_web_page import RiseSetWebPage

SCRIPTDIR = pathlib.Path(__file__).parent.resolve()
INPUTDIR = SCRIPTDIR.joinpath("input-file")


class TestGetPlace:
    def test_get_place(self):
        # given
        file = INPUTDIR.joinpath("m1301.html").resolve()
        text = file.read_text(encoding='euc-jp')
        page = RiseSetWebPage(text=text)

        # when
        place = page.get_place()

        # then
        assert place == '東京(東京都): Tokyo'


class Test_GetYearMonthStr:
    def test__get_year_month_str(self):
        # given
        file = INPUTDIR.joinpath("m1301.html").resolve()
        text = file.read_text(encoding='euc-jp')
        page = RiseSetWebPage(text=text)

        # when
        ym_str = page._get_year_month_str()

        # then
        assert ym_str == '2023年  1月'


class TestGetRiseSetEvent:
    def test_get_rise_set_event(self):
        # given
        file = INPUTDIR.joinpath("m1301.html").resolve()
        text = file.read_text(encoding='euc-jp')
        page = RiseSetWebPage(text=text)

        # when
        events = page.get_rise_set_event()

        # then
        assert len(events) == 60
