# simple-dashboard-py

Sample project to create simple dashboard html files

## usage

web からデータを取得して、ダッシュボードの html ファイルを作成するサンプルプロジェクト。

下記コマンドでお試しダッシュボードを作成可能。

```python
import naoj
naoj.WebToUptimeSimpleDashboard().execute(
    path="path-of-your-output-file.html",
)
```

ダッシュボードはネットにつながる環境であればブラウザで表示できるはず。

自身のダッシュボードを作成したい場合にやるべきことを挙げる ([class-diagram](#class-diagram) も参照) 。

- フォルダ `koyomi` を自身のデータ編集ロジックに置き換え、そこから `simple-dashboard` を使用する。
- ダッシュボードのレイアウトを変更したい場合は `KpiSideBySideHtmlWriter` が読み込んでいる jinja2 テンプレート `kpi_side_by_side_html_writer.j2.html` を変更する。
- ダッシュボードに表示するデータを変更したい場合やダッシュボードのレイアウトを増やしたい場合は `KpiSideBySideHtmlWriter` と `kpi_side_by_side_html_writer.j2.html` をコピーして新しい writer を作成／使用する。

## class-diagram

```mermaid
flowchart BT
subgraph class-diagram
    direction TB
    subgraph naoj
        subgraph koyomi
            subgraph entity
                RiseSetEvent["
                    RiseSetEvent
                    ---
                    datetime
                    place
                    event
                "]
                RiseSetEventList["
                    RiseSetEventCollection
                    ---
                    to_uptime()
                "]
                Uptime["
                    Uptime
                    ---
                    place
                    rise
                    set
                "]
                UptimeList
                RiseSetEventList --> RiseSetEvent
                RiseSetEventList --> UptimeList
                RiseSetEventList --> Uptime
                UptimeList --> Uptime
            end
            subgraph web
                RiseSetWebPageUrl
                RiseSetWebPage
                RiseSetWebRepository
                RiseSetWebRepository --> RiseSetWebPageUrl
                RiseSetWebRepository --> RiseSetWebPage
            end
            subgraph writer
                UptimeDashboardWriter
            end
            subgraph usecase
                WebToUptimeSimpleDashboard
            end
            RiseSetWebRepository --> RiseSetEventList
            RiseSetWebPage --> RiseSetEvent
            RiseSetWebPage --> RiseSetEventList
            UptimeDashboardWriter --> UptimeList
            WebToUptimeSimpleDashboard --> RiseSetWebRepository
            WebToUptimeSimpleDashboard --> UptimeDashboardWriter
        end
        subgraph simple_dashboard
            Jinja2HtmlWriter
            KpiSideBySideHtmlWriter
            KpiSideBySideHtmlWriter --> Jinja2HtmlWriter
        end
        UptimeDashboardWriter --> KpiSideBySideHtmlWriter
    end
    UptimeDashboardWriter --> plotly
    Jinja2HtmlWriter --> jinja2
end
```

## thanks to

Data in this project are from National Astronomical Observatory of Japan.

国立天文台ホームページよりデータを引用させていただいています。ありがとうございます！

[サイト利用規定：国立天文台 暦計算室](https://eco.mtk.nao.ac.jp/koyomi/site/)

> 各ページの内容は、ご自由に利用ください。ご利用にあたり、届出の必要はございません。
> 引用にあたっては、「国立天文台ホームページより引用」、「(C) 国立天文台」、「(C) NAOJ」等の表記やリンク設定をしていただければ幸いです。


## Cautions on using devcontainer

This project comes with devcontainer but it may not work well with VSCode. For example, an error occurs at "TESTING" in the activity bar.

Here's how to fix it.

1. At first, Pytest Discovery Error occurs at "TESTING".
    ![cautions-on-using-devcontainer_001.png](./docs/img/cautions-on-using-devcontainer_001.png)
2. Press Ctrl + Shift + P for command palette, and select "Python: Select Interpreter".
    ![cautions-on-using-devcontainer_002.png](./docs/img/cautions-on-using-devcontainer_002.png)
3. It shows there are 2 python interpreters, and VSCode is using the wrong one. Select version 3.7 (in the picture, it would be "Python 3.7.16 64-bit").
    ![cautions-on-using-devcontainer_003.png](./docs/img/cautions-on-using-devcontainer_003.png)
4. Get batck to the "TESTING" and press "Refresh Tests". The error is gone.
    ![cautions-on-using-devcontainer_004.png](./docs/img/cautions-on-using-devcontainer_004.png)
