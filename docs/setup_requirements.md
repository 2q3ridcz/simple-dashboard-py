# Setup under a restricted condetion

This is just a private note. No need to read to understand the `simple-dashboard-py`.

## background

`simple-dashboard-py` is originally created for use under a restricted condition where:

- One version of Anaconda is allowed to use (conda 4.7.10, Python 3.7.3).
- This anaconda does not contain plotly.
- Neither `conda install` nor `pip install` command is allowed.

To use plotly in this condition, I:

1. Downloaded plotly and its required packages from github and extract the zip file.
2. Added their path to environment path in top-level `__init__.py`.

## codes

First is `requirements/setup_requirements.py`, a script to download plotly and its required packages from github and extract the zip file. (`requirements` is a folder in the project root.)

```python
'''Script to setup plotly and its required packages

This script downloads plotly and its required packages from github
and extracts the zip file, to be able to use plotly without 
`conda install` and `pip install`.
'''
import pathlib
import requests
import zipfile


SELF_FILE = pathlib.Path(__file__).resolve()
OUT_FOLDER = SELF_FILE.parent

url_list = [
    "https://github.com/plotly/plotly.py/archive/refs/tags/v5.13.0.zip",
    "https://github.com/jd/tenacity/archive/refs/tags/8.1.0.zip",
]

for url in url_list:
    out_file = OUT_FOLDER.joinpath(url.split("/")[-1])
    res = requests.get(url)
    out_file.write_bytes(res.content)

    with zipfile.ZipFile(file=out_file, mode="r") as zip:
        zip.extractall(path=OUT_FOLDER)
```

Second is `naoj/__init__.py`, a script to add path of these packages to environment path.

```python
'''Top-level __init__.py file in naoj package

Besides ordinary use of __init__.py, this script adds path of plotly and its
required packages to environment path. To use plotly without `conda install`
and `pip install`, make sure these packages are present at:
- requirements/tenacity-8.1.0
- requirements/plotly.py-5.13.0/packages/python/plotly

If plotly is installed via `conda install` or `pip install`, you don't have to
do anything.
'''
import sys
import pathlib

SELF_FILE = pathlib.Path(__file__).resolve()
SELF_FOLDER = SELF_FILE.parent

[sys.path.append(str(pathlib.Path(p).resolve())) for p in [
    SELF_FOLDER.joinpath("../requirements/tenacity-8.1.0"),
    SELF_FOLDER.joinpath("../requirements/plotly.py-5.13.0/packages/python/plotly"),
] if str(pathlib.Path(p).resolve()) not in sys.path]

from .koyomi.usecase.web_to_uptime_simple_dashboard import WebToUptimeSimpleDashboard
```