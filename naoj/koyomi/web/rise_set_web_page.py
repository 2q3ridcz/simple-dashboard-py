from bs4 import BeautifulSoup
import pandas as pd

from ..entity.rise_set_event import RiseSetEvent
from ..entity.rise_set_event_list import RiseSetEventList


class RiseSetWebPage:
    text: str

    def __init__(
        self,
        text: str
    ):
        self.text = text

    def to_soup(self) -> BeautifulSoup:
        text = self.text
        return BeautifulSoup(text, features="lxml")

    def get_place(self) -> str:
        soup = self.to_soup()
        tag = soup.h2
        if tag is None:
            raise ValueError("Place not found.")
        return tag.text

    def _get_year_month_str(self) -> str:
        soup = self.to_soup()
        tag = soup.caption
        if tag is None:
            raise ValueError("YearMonth not found.")
        return tag.text

    def _get_rise_set_raw_df(self) -> pd.DataFrame:
        text = self.text
        df_list = pd.read_html(text)
        df = df_list[0]
        return df

    def get_rise_set_df(self) -> pd.DataFrame:
        place = self.get_place()
        ym_str = self._get_year_month_str()
        df = self._get_rise_set_raw_df()

        # '出' の DataFrame を作成
        rise_df = df.loc[:, ['日', '出']]
        rise_df.columns = ['日', 'time_str']
        rise_df['event'] = 'rise'
        rise_df = rise_df[rise_df['time_str'] != '--:--']

        # '入り' の DataFrame を作成
        set_df = df.loc[:, ['日', '入り']]
        set_df.columns = ['日', 'time_str']
        set_df['event'] = 'set'
        set_df = set_df[set_df['time_str'] != '--:--']

        # '出' と '入り' を concat
        df = pd.concat([rise_df, set_df])
        df['date_str'] = df['日'].map(lambda x: f"{ym_str}{x}日")
        df['datetime_str'] = df['date_str'] + " " + df['time_str']
        df['datetime'] = pd.to_datetime(
            df['datetime_str'],
            format='%Y年 %m月%d日 %H:%M',
        )
        df = df.sort_values(by='datetime')
        df['place'] = place
        return df.loc[:, ['datetime', 'place', 'event']]

    def get_rise_set_event(self) -> RiseSetEventList:
        df = self.get_rise_set_df()
        events = RiseSetEventList()
        for x in df.to_dict(orient="records"):
            events.append(RiseSetEvent(
                datetime=x['datetime'].to_pydatetime(),
                place=x['place'],
                event=x['event'],
            ))
        return events
