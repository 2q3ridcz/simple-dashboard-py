import datetime
import requests

from .rise_set_web_page_url import RiseSetWebPageUrl
from .rise_set_web_page import RiseSetWebPage
from ..entity.rise_set_event_list import RiseSetEventList


class RiseSetWebRepository:
    def get_moon_rise_set_events(
        self,
        # star: str,
        # start: datetime.datetime,
        # end: datetime.datetime,
    ) -> RiseSetEventList:
        '''国立天文台ホームページより 月の出入りデータを取得する

        機能を作りこんでいないため、現在は 2023年01月の東京のデータのみ取得可能。
        '''
        date = datetime.datetime(2023, 1, 1)

        events = RiseSetEventList()
        url = RiseSetWebPageUrl().get_moon_url(date=date)
        res = requests.get(url, timeout=1)
        # TODO: 国立天文台ホームページはページごとに encoding が違うっぽい…
        res.encoding = 'euc-jp'
        text = res.text
        page = RiseSetWebPage(text=text)
        events += page.get_rise_set_event()
        # TODO: filter
        return events
