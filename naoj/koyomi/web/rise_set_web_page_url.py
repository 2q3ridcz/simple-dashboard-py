import datetime


class RiseSetWebPageUrl:
    def get_moon_url(self, date: datetime.datetime) -> str:
        month_str = str(date.month).ljust(2, '0')
        url = (
            "https://eco.mtk.nao.ac.jp/koyomi/"
            f"dni/{date.year}/m13{month_str}.html"
        )
        return url

    def get_sun_url(self, date: datetime.datetime) -> str:
        month_str = str(date.month).ljust(2, '0')
        url = (
            "https://eco.mtk.nao.ac.jp/koyomi/"
            f"dni/{date.year}/s13{month_str}.html"
        )
        return url
