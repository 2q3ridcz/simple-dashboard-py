import datetime
import math

import pandas as pd
import plotly.express as px


from ..entity.uptime_list import UptimeList
from ...simple_dashboard.kpi_side_by_side_html_writer import KpiSideBySideHtmlWriter


class UptimeDashboardWriter():
    def __init__(
        self,
        uptimes: UptimeList,
    ):
        self.uptimes = uptimes

    def write(
        self,
        path: str,
    ):
        uptimes = self.uptimes
        df = pd.DataFrame(data=[x.__dict__ for x in uptimes])
        df['uptime'] = [x.uptime().total_seconds() / 60 / 60 for x in uptimes]
        df['rise_date'] = pd.to_datetime(df['rise'].dt.date)
        df['rise_time'] = (df['rise'] - df['rise_date']).map(lambda x: datetime.datetime(1, 1, 1) + x)
        df['set_date'] = pd.to_datetime(df['set'].dt.date)
        df['set_time'] = (df['set'] - df['set_date']).map(lambda x: datetime.datetime(1, 1, 1) + x)

        graphs = []

        # graph #1
        fig = px.line(df, x="rise_date", y="rise_time")
        fig.update_layout(
            plot_bgcolor="white",
            margin={"pad": 0, "l": 40, "r": 40, "t": 40, "b": 40},
        )
        fig.update_traces(line_color="blue")
        fig.update_xaxes(
            title=None,
            showline=False,
            showgrid=False,
            type="date",
            zeroline=False,
        )
        fig.update_yaxes(
            title=None,
            tickformat="%H:%M",
            showgrid=False,
            showline=False,
            zeroline=False,
        )

        graphs.append({
            'title': 'rise time',
            'value': df['rise_time'].iloc[-1].strftime('%H:%M'),
            'div': fig.to_html(include_plotlyjs='cdn', full_html=False),
        })

        # graph #2
        fig = px.line(df, x="set_date", y="set_time")
        fig.update_layout(
            plot_bgcolor="white",
            margin={"pad": 0, "l": 40, "r": 40, "t": 40, "b": 40},
        )
        fig.update_traces(line_color="blue")
        fig.update_xaxes(
            title=None,
            showline=False,
            showgrid=False,
            type="date",
            zeroline=False,
        )
        fig.update_yaxes(
            title=None,
            tickformat="%H:%M",
            showgrid=False,
            showline=False,
            zeroline=False,
        )

        graphs.append({
            'title': 'set time',
            'value': df['set_time'].iloc[-1].strftime('%H:%M'),
            'div': fig.to_html(include_plotlyjs='cdn', full_html=False),
        })

        # graph #3
        fig = px.line(df, x="rise_date", y="uptime")
        fig.update_layout(
            plot_bgcolor="white",
            margin={"pad": 0, "l": 40, "r": 40, "t": 40, "b": 40},
        )
        fig.update_traces(line_color="blue")
        fig.update_xaxes(
            title=None,
            showline=False,
            showgrid=False,
            type="date",
            zeroline=False,
        )
        fig.update_yaxes(
            title=None,
            tickformat="%H:%M",
            showgrid=False,
            showline=False,
            zeroline=False,
        )

        graphs.append({
            'title': 'uptime',
            'value': f"{df['uptime'].iloc[-1]:.1f}",
            'unit': '時間',
            'div': fig.to_html(include_plotlyjs='cdn', full_html=False),
        })

        # write
        kpis = [
            graphs[0],
            graphs[1],
            graphs[2],
        ]
        data = {
            'title': 'uptime of the moon',
            'h1': 'uptime of the moon at Tokyo',
            'pre_dashboard': None,
            'post_dashboard': None,
            'kpis': kpis,
            'kpi_width': math.ceil(100 / len(kpis)),
        }
        KpiSideBySideHtmlWriter().write(
            path=path,
            data=data,
        )
