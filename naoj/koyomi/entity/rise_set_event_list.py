"""Listlike object for RiseSetEvent

Inherits list class. See here for inheriting list:
https://realpython.com/inherit-python-list/
"""
from itertools import groupby

from .rise_set_event import RiseSetEvent
from .uptime import Uptime
from .uptime_list import UptimeList


class RiseSetEventList(list):
    def __init__(self, iterable=None):
        if iterable is None:
            iterable = []
        super().__init__(self._validate_item(item) for item in iterable)

    def __setitem__(self, index, item):
        super().__setitem__(index, self._validate_item(item))

    def insert(self, index, item):
        super().insert(index, self._validate_item(item))

    def append(self, item):
        super().append(self._validate_item(item))

    def extend(self, other):
        if isinstance(other, type(self)):
            super().extend(other)
        else:
            super().extend(self._validate_item(item) for item in other)

    def _validate_item(self, value):
        if isinstance(value, RiseSetEvent):
            return value
        raise TypeError(
            f"RiseSetEvent object expected, got {type(value).__name__}"
        )

    def __add__(self, other):
        return super().__add__(
            [self._validate_item(item) for item in other]
        )

    # Does not support __radd__ because list does not support it.
    # def __radd__(self, other):

    def __iadd__(self, other):
        return super().__iadd__(
            [self._validate_item(item) for item in other]
        )

    def to_uptime(self) -> UptimeList:
        uptimes = UptimeList()
        std = sorted(self, key=lambda x: (x.place, x.datetime))
        for place, group in groupby(std, key=lambda x: x.place):
            first_record = True
            rise = None
            for x in group:
                if x.event == "rise":
                    if rise is not None:
                        raise ValueError("rise again without setting")
                    rise = x
                elif x.event == "set":
                    if first_record:
                        # set が 1 レコード目にある場合は月跨ぎと判断してそのレコードを Skip。
                        first_record = False
                        continue
                    first_record = False
                    if rise is None:
                        raise ValueError("set again without rising")
                    uptimes.append(Uptime(
                        place=place,
                        rise=rise.datetime,
                        set=x.datetime,
                    ))
                    rise = None
        return uptimes
