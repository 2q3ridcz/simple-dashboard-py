import datetime
from dataclasses import dataclass


@dataclass(init=True, eq=True, frozen=True)
class Uptime:
    place: str
    # star: str
    rise: datetime.datetime
    set: datetime.datetime

    def uptime(self) -> datetime.timedelta:
        return self.set - self.rise
