"""Listlike object for Uptime

Inherits list class. See here for inheriting list:
https://realpython.com/inherit-python-list/
"""
from .uptime import Uptime


class UptimeList(list):
    def __init__(self, iterable=None):
        if iterable is None:
            iterable = []
        super().__init__(self._validate_item(item) for item in iterable)

    def __setitem__(self, index, item):
        super().__setitem__(index, self._validate_item(item))

    def insert(self, index, item):
        super().insert(index, self._validate_item(item))

    def append(self, item):
        super().append(self._validate_item(item))

    def extend(self, other):
        if isinstance(other, type(self)):
            super().extend(other)
        else:
            super().extend(self._validate_item(item) for item in other)

    def _validate_item(self, value):
        if isinstance(value, Uptime):
            return value
        raise TypeError(
            f"Uptime object expected, got {type(value).__name__}"
        )

    def __add__(self, other):
        return super().__add__(
            [self._validate_item(item) for item in other]
        )

    # Does not support __radd__ because list does not support it.
    # def __radd__(self, other):

    def __iadd__(self, other):
        return super().__iadd__(
            [self._validate_item(item) for item in other]
        )
