import datetime
from dataclasses import dataclass


@dataclass(init=True, eq=True, frozen=True)
class RiseSetEvent:
    datetime: datetime.datetime
    place: str
    # star: str
    event: str
