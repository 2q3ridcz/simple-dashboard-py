from ..web.rise_set_web_repository import RiseSetWebRepository
from ..writer.uptime_dashboard_writer import UptimeDashboardWriter


class WebToUptimeSimpleDashboard():
    def execute(
        self,
        path: str,
    ):
        repo = RiseSetWebRepository()
        events = repo.get_moon_rise_set_events()
        uptimes = events.to_uptime()
        UptimeDashboardWriter(
            uptimes=uptimes
        ).write(
            path=path,
        )
