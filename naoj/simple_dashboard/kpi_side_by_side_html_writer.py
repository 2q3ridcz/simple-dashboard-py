import pathlib
import typing

from .jinja2_html_writer import Jinja2HtmlWriter

SELF_FILE = pathlib.Path(__file__).resolve()
SELF_FOLDER = SELF_FILE.parent


class KpiSideBySideHtmlWriter():
    template: str = str(SELF_FOLDER.joinpath("kpi_side_by_side_html_writer.j2.html"))

    def write(
        self,
        path: str,
        data: typing.Dict,
    ):
        template = self.template
        Jinja2HtmlWriter().write(
            path=path,
            template=template,
            data=data,
        )
