'''Top-level __init__.py file in naoj package

Besides ordinary use of __init__.py, this script adds path of plotly and its
required packages to environment path. To use plotly without `conda install`
and `pip install`, make sure these packages are present at:
- requirements/tenacity-8.1.0
- requirements/plotly.py-5.13.0/packages/python/plotly

If plotly is installed via `conda install` or `pip install`, you don't have to
do anything.
'''
import sys
import pathlib

SELF_FILE = pathlib.Path(__file__).resolve()
SELF_FOLDER = SELF_FILE.parent

[sys.path.append(str(pathlib.Path(p).resolve())) for p in [
    SELF_FOLDER.joinpath("../requirements/tenacity-8.1.0"),
    SELF_FOLDER.joinpath("../requirements/plotly.py-5.13.0/packages/python/plotly"),
] if str(pathlib.Path(p).resolve()) not in sys.path]

from .koyomi.usecase.web_to_uptime_simple_dashboard import WebToUptimeSimpleDashboard
